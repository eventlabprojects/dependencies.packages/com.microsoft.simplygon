# Third party licenses

These are the third party licenses used by the Simplygon product:


#### 3ds Max SDK
[https://www.autodesk.com/developer-network/platform-technologies/3ds-max](https://www.autodesk.com/developer-network/platform-technologies/3ds-max)

#### Alembic
[https://github.com/alembic/alembic](https://github.com/alembic/alembic)  
License: [https://github.com/alembic/alembic/blob/master/LICENSE.txt](https://github.com/alembic/alembic/blob/master/LICENSE.txt)

#### Azure Storage Client Library
[https://github.com/Azure/azure-storage-cpp](https://github.com/Azure/azure-storage-cpp)  
License: [https://github.com/Azure/azure-storage-cpp/blob/master/LICENSE.txt](https://github.com/Azure/azure-storage-cpp/blob/master/LICENSE.txt)

#### Boost
[https://github.com/boostorg/boost](https://github.com/boostorg/boost)  
License: [https://github.com/boostorg/boost/blob/master/LICENSE_1_0.txt](https://github.com/boostorg/boost/blob/master/LICENSE_1_0.txt)

#### Costura.Fody
[https://github.com/Fody/Costura](https://github.com/Fody/Costura)  
License: [https://github.com/Fody/Costura/blob/master/LICENSE](https://github.com/Fody/Costura/blob/master/LICENSE)

#### cpprestsdk
[https://github.com/microsoft/cpprestsdk](https://github.com/microsoft/cpprestsdk)  
License: [https://github.com/microsoft/cpprestsdk/blob/master/license.txt](https://github.com/microsoft/cpprestsdk/blob/master/license.txt)

#### DirectXTex
[https://github.com/microsoft/DirectXTex](https://github.com/microsoft/DirectXTex)  
License: [https://github.com/microsoft/DirectXTex/blob/master/LICENSE](https://github.com/microsoft/DirectXTex/blob/master/LICENSE)

#### Eigen
[https://eigen.tuxfamily.org/index.php?title=Main_Page](https://eigen.tuxfamily.org/index.php?title=Main_Page)  
License: [https://www.mozilla.org/en-US/MPL/2.0](https://www.mozilla.org/en-US/MPL/2.0)

#### Embree
[https://www.embree.org](https://www.embree.org)  
License: [https://www.apache.org/licenses/](https://www.apache.org/licenses/)

#### FBX SDK
[http://usa.autodesk.com/adsk/servlet/pc/item?siteID=123112&id=10775847](http://usa.autodesk.com/adsk/servlet/pc/item?siteID=123112&id=10775847)

#### FreeImage
[https://freeimage.sourceforge.io](https://freeimage.sourceforge.io)  
License: [https://freeimage.sourceforge.io/freeimage-license.txt](https://freeimage.sourceforge.io/freeimage-license.txt)

#### Fody
[https://github.com/Fody/Fody](https://github.com/Fody/Fody)  
License: [https://github.com/Fody/Fody/blob/master/License.txt](https://github.com/Fody/Fody/blob/master/License.txt)

#### glTF-SDK
[https://github.com/microsoft/glTF-SDK](https://github.com/microsoft/glTF-SDK)  
License: [https://github.com/microsoft/glTF-SDK/blob/master/LICENSE](https://github.com/microsoft/glTF-SDK/blob/master/LICENSE)

#### Ini-parser
[https://github.com/rickyah/ini-parser](https://github.com/rickyah/ini-parser)  
License: [https://github.com/rickyah/ini-parser/blob/master/LICENSE](https://github.com/rickyah/ini-parser/blob/master/LICENSE)

#### JsonCPP
[https://github.com/open-source-parsers/jsoncpp](https://github.com/open-source-parsers/jsoncpp)  
License: [https://github.com/open-source-parsers/jsoncpp/blob/master/LICENSE](https://github.com/open-source-parsers/jsoncpp/blob/master/LICENSE)

#### JSON/SJSON parser
[https://github.com/mjansson/json](https://github.com/mjansson/json)  
License: [https://github.com/mjansson/json/blob/master/LICENSE](https://github.com/mjansson/json/blob/master/LICENSE)

#### jxrLib
[https://github.com/4creators/jxrlib](https://github.com/4creators/jxrlib)  
License: [https://github.com/4creators/jxrlib/blob/master/LICENSE](https://github.com/4creators/jxrlib/blob/master/LICENSE)

#### Libcurl
[https://curl.se](https://curl.se)  
License: [https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)

#### LibJpeg
This software is based in part on the work of the Independent JPEG Group.
[https://www.ijg.org](https://www.ijg.org)  
License: [https://jpegclub.org/reference/libjpeg-license/](https://jpegclub.org/reference/libjpeg-license/)

#### LibPNG
[http://www.libpng.org/pub/png/libpng.html](http://www.libpng.org/pub/png/libpng.html)  
License: [http://www.libpng.org/pub/png/src/libpng-LICENSE.txt](http://www.libpng.org/pub/png/src/libpng-LICENSE.txt)

#### LibRaw
[https://github.com/LibRaw/LibRaw](https://github.com/LibRaw/LibRaw)  
License: [https://github.com/LibRaw/LibRaw/blob/master/LICENSE.CDDL](https://github.com/LibRaw/LibRaw/blob/master/LICENSE.CDDL)

#### LibTiff
[https://gitlab.com/libtiff/libtiff](https://gitlab.com/libtiff/libtiff)  
License: [https://gitlab.com/libtiff/libtiff/-/blob/master/COPYRIGHT](https://gitlab.com/libtiff/libtiff/-/blob/master/COPYRIGHT)

#### LibWebp
[https://github.com/webmproject/libwebp](https://github.com/webmproject/libwebp)  
License: [https://github.com/webmproject/libwebp/blob/master/COPYING](https://github.com/webmproject/libwebp/blob/master/COPYING)

#### Lz4
[https://github.com/lz4/lz4](https://github.com/lz4/lz4)  
License: [https://github.com/lz4/lz4/blob/dev/LICENSE](https://github.com/lz4/lz4/blob/dev/LICENSE)

#### Maya SDK
[https://apps.autodesk.com/MAYA/en/Detail/Index?id=6303159649350432165&os=Win64&appLang=en](https://apps.autodesk.com/MAYA/en/Detail/Index?id=6303159649350432165&os=Win64&appLang=en)

#### Maya-Net-Wpf-DarkScheme
[https://github.com/ADN-DevTech/Maya-Net-Wpf-DarkScheme](https://github.com/ADN-DevTech/Maya-Net-Wpf-DarkScheme)
License: [https://github.com/ADN-DevTech/Maya-Net-Wpf-DarkScheme/blob/master/LICENSE](https://github.com/ADN-DevTech/Maya-Net-Wpf-DarkScheme/blob/master/LICENSE)

#### mDNS/DNS-SD library
[https://github.com/mjansson/mdns](https://github.com/mjansson/mdns)  
License: [https://github.com/mjansson/mdns/blob/master/LICENSE](https://github.com/mjansson/mdns/blob/master/LICENSE)

#### Mersenne Twister
[http://www.math.sci.hiroshima-u.ac.jp/m-mat/MT/emt.html](http://www.math.sci.hiroshima-u.ac.jp/m-mat/MT/emt.html)  
License: [https://opensource.org/licenses/BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)

#### MikkTSpace
[https://github.com/mmikk/MikkTSpace](https://github.com/mmikk/MikkTSpace)  
License: [https://github.com/mmikk/MikkTSpace/blob/master/mikktspace.h](https://github.com/mmikk/MikkTSpace/blob/master/mikktspace.h)

#### Newtonsoft Json
[https://github.com/JamesNK/Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json)  
License: [https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md](https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md)

#### Openexr
[https://github.com/AcademySoftwareFoundation/openexr/tree/master/IlmBase](https://github.com/AcademySoftwareFoundation/openexr/tree/master/IlmBase)  
License: [https://www.openexr.com/license.html](https://www.openexr.com/license.html)

#### Open Jpeg
[https://github.com/uclouvain/openjpeg](https://github.com/uclouvain/openjpeg)  
License: [https://github.com/uclouvain/openjpeg/blob/master/LICENSE](https://github.com/uclouvain/openjpeg/blob/master/LICENSE)

#### Open Numeric Library
[https://gforge.inria.fr/projects/opennl](https://gforge.inria.fr/projects/opennl)  
License: [https://opensource.org/licenses/BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)

#### OpenSSL
[https://www.openssl.org](https://www.openssl.org)  
License: [https://www.openssl.org/source/apache-license-2.0.txt](https://www.openssl.org/source/apache-license-2.0.txt)

#### rpmalloc
[https://github.com/mjansson/rpmalloc](https://github.com/mjansson/rpmalloc)  
License: [https://github.com/mjansson/rpmalloc/blob/develop/LICENSE](https://github.com/mjansson/rpmalloc/blob/develop/LICENSE)

#### SHA256
[http://www.ouah.org/ogay/sha2/](http://www.ouah.org/ogay/sha2/)  
License: [https://opensource.org/licenses/BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)

#### SysInfo
[https://github.com/da0x/xr.engine/tree/master/src/Sysinfo](https://github.com/da0x/xr.engine/tree/master/src/Sysinfo)  
License: [https://github.com/da0x/xr.engine/blob/master/LICENSE](https://github.com/da0x/xr.engine/blob/master/LICENSE)

#### TBB
[https://github.com/oneapi-src/oneTBB](https://github.com/oneapi-src/oneTBB)  
License: [https://github.com/oneapi-src/oneTBB/blob/tbb_2020/LICENSE](https://github.com/oneapi-src/oneTBB/blob/tbb_2020/LICENSE)

#### TTMath
[https://www.ttmath.org](https://www.ttmath.org)  
License: [https://opensource.org/licenses/BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)

#### TinyXML
[https://sourceforge.net/projects/tinyxml/?source=directory](https://sourceforge.net/projects/tinyxml/?source=directory)  
License: [https://sourceforge.net/directory/os:windows/license:zlib/](https://sourceforge.net/directory/os:windows/license:zlib/)

#### USD
[https://github.com/PixarAnimationStudios/USD](https://github.com/PixarAnimationStudios/USD)  
License: [https://github.com/PixarAnimationStudios/USD/blob/release/LICENSE.txt](https://github.com/PixarAnimationStudios/USD/blob/release/LICENSE.txt)

#### VTK
[https://vtk.org](https://vtk.org)  
License: [https://opensource.org/licenses/BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)

#### ZLib
[https://github.com/madler/zlib](https://github.com/madler/zlib)  
License: [https://github.com/madler/zlib](https://github.com/madler/zlib)










